/*
 * RequestHandler.h
 *
 *  Created on: 15 Mar 2019
 *      Author: sgasper
 */

#ifndef DEBUGNETRESTAPI_H_
#define DEBUGNETRESTAPI_H_

#include <iostream>
using namespace std;

#include "pistache/http.h"
#include <pistache/http_headers.h>
#include "pistache/router.h"
#include "pistache/endpoint.h"
#include <pistache/net.h>

#include "debugnet-analysis.h"

using namespace Pistache;
using namespace Rest;


// Map the result code from processing a query to a HTTP result code
const map<QUERY_RESULT_CODE, Http::Code> QRC_TO_HTTP_CODE = {
		{QUERY_SUCCESS, Http::Code::Ok},
		{QUERY_ERR, Http::Code::Bad_Request},
		{QUERY_ERR_DEVICE_NOT_FOUND, Http::Code::Bad_Request}
};

class DebugnetRestAPI {
public:
	string addColonsToMac(string macAddr);
	void runConnectionDropTS(const Rest::Request& request,
				Http::ResponseWriter response);
	void runSlowDeviceTS(const Rest::Request& request,
				Http::ResponseWriter response);
	void runSlowNetworkTS(const Rest::Request& request,
			Http::ResponseWriter response);
	void runGetCurrentInternetSpeed(const Rest::Request& request,
				Http::ResponseWriter response);
	void runGetTimeOfDayInternetFastestSlowest(const Rest::Request& request,
			Http::ResponseWriter response);
	void runGetIspPerformance(const Rest::Request& request,
			Http::ResponseWriter response);
	void getDevices(const Rest::Request& request, Http::ResponseWriter response);
};

#endif /* DEBUGNETRESTAPI_H_ */
