/*
 * debugnet-rest-server.h
 *
 *  Created on: 7 Mar 2019
 *      Author: sgasper
 */

#ifndef DEBUGNET_REST_SERVER_H_
#define DEBUGNET_REST_SERVER_H_

#include <iostream>
using namespace std;

#include "pistache/http.h"
#include "pistache/router.h"
#include "pistache/endpoint.h"
using namespace Pistache;
using namespace Rest;

const int PORT = 9080;	//Port to bind server to

#include "DebugnetRestAPI.h"


#endif /* DEBUGNET_REST_SERVER_H_ */
