/*
 * RequestHandler.cpp
 *
 *  Created on: 15 Mar 2019
 *      Author: sgasper
 */

#include "DebugnetRestAPI.h"

/*
 *Adds columns to a mac address
 */
string DebugnetRestAPI::addColonsToMac(string macAddr){
	for (unsigned int i = 2; i < macAddr.size(); i+=3){
		macAddr.insert(i, ":");
	}
	return macAddr;
}

/*
 * Service resource call for trouble shooting connection drops to a device
 */
void DebugnetRestAPI::runConnectionDropTS(const Rest::Request& request,
			Http::ResponseWriter response){
	Http::Code exitCode;
	string body;

	string deviceMac = request.param(":device-mac").as<string>();
	int slowTime = request.param(":instable-time").as<int>();

	// Fetch the device in question, respond with an error if no device exists
	// with the MAC address.
	string deviceMacWithColons = addColonsToMac(deviceMac);
	Device device = getDevice(deviceMacWithColons);
	if (device.isNull() == true){
		exitCode = Http::Code::Bad_Request;
		body = "no device exists with MAC address " + deviceMacWithColons;
		response.headers().add<Http::Header::ContentType>(MIME(Text, Plain));
	}
	else{
		// Run troubleshoot
		TSQueryResult queryResult = queryWhyDoesConnectionDrop(device, slowTime);

		// Fail the resource request if their was an issue executing the query.
		if (queryResult.code != QUERY_SUCCESS){
			// Map the query result code to a REST exit code and produce an error
			// message.
			response.headers().add<Http::Header::ContentType>(MIME(Text, Plain));
			body = queryResult.err;
			exitCode = QRC_TO_HTTP_CODE.find(queryResult.code)->second;
		}
		else{
			// Get troubleshoot report in JSON format
			response.headers().add<Http::Header::ContentType>(MIME(Application, Json));
			body = queryResult.report.getAsJsonStr();
			exitCode = Http::Code::Ok;
		}
	}

	response.send(exitCode, body);
}

/*
 * Service resource call for trouble shooting slow internet to a device
 */
void DebugnetRestAPI::runSlowDeviceTS(const Rest::Request& request,
			Http::ResponseWriter response){
	Http::Code exitCode;
	string body;

	string deviceMac = request.param(":device-mac").as<string>();
	int slowTime = request.param(":slow-time").as<int>();

	// Fetch the device in question, respond with an error if no device exists
	// with the MAC address.
	string deviceMacWithColons = addColonsToMac(deviceMac);
	Device device = getDevice(deviceMacWithColons);
	if (device.isNull() == true){
		exitCode = Http::Code::Bad_Request;
		body = "no device exists with MAC address " + deviceMacWithColons;
	}
	else{
		// Run troubleshoot
		TSQueryResult queryResult = queryWhyIsDeviceSlow(device, slowTime);

		// Fail the resource request if their was an issue executing the query.
		if (queryResult.code != QUERY_SUCCESS){
			// Map the query result code to a REST exit code and produce an error
			// message.
			response.headers().add<Http::Header::ContentType>(MIME(Text, Plain));
			body = queryResult.err;
			exitCode = QRC_TO_HTTP_CODE.find(queryResult.code)->second;
		}
		else{
			// Get troubleshoot report in JSON format
			response.headers().add<Http::Header::ContentType>(MIME(Application, Json));
			body = queryResult.report.getAsJsonStr();
			exitCode = Http::Code::Ok;
		}
	}
	response.send(exitCode, body);
}

/*
 * Service resource call for trouble shooting slow networks
 */
void DebugnetRestAPI::runSlowNetworkTS(const Rest::Request& request,
			Http::ResponseWriter response){
	Http::Code exitCode;
	string body;
	int slowTime = request.param(":slow-time").as<int>();

	// Run troubleshoot
	TSQueryResult queryResult = queryWhyIsEntireNetworkSlow(slowTime);

	// Fail the resource request if their was an issue executing the query.
	if (queryResult.code != QUERY_SUCCESS){
		// Map the query result code to a REST exit code and produce an error
		// message.
		response.headers().add<Http::Header::ContentType>(MIME(Text, Plain));
		body = queryResult.err;
		exitCode = QRC_TO_HTTP_CODE.find(queryResult.code)->second;
	}
	else{
		// Get troubleshoot report in JSON format
		response.headers().add<Http::Header::ContentType>(MIME(Application, Json));
		body = queryResult.report.getAsJsonStr();
		exitCode = Http::Code::Ok;
	}

	response.send(exitCode, body);
}

/*
 * Service resource call for factual query off 'what is the current internet speed?'
 */
void DebugnetRestAPI::runGetCurrentInternetSpeed(const Rest::Request& request,
			Http::ResponseWriter response){
	Http::Code exitCode;
	string body;

	response.headers().add<Http::Header::ContentType>(MIME(Text, Plain));

	// Run factual query
	FactualQueryResult queryResult = queryGetCurrentInternetSpeed();

	// Fail the resource request if their was an issue executing the query.
	if (queryResult.code != QUERY_SUCCESS){
		// Map the query result code to a REST exit code and produce an error
		// message.
		body = queryResult.err;
		exitCode = QRC_TO_HTTP_CODE.find(queryResult.code)->second;
	}
	else{
		body = queryResult.factualStr;
		exitCode = Http::Code::Ok;
	}

	response.send(exitCode, body);
}

/*
 * Service resource call for factual query off 'what time of day internet is
 * fastest/slowest?'.
 */
void DebugnetRestAPI::runGetTimeOfDayInternetFastestSlowest(
		const Rest::Request& request, Http::ResponseWriter response){
	Http::Code exitCode;
	string body;

	response.headers().add<Http::Header::ContentType>(MIME(Text, Plain));

	// Run factual query
	FactualQueryResult queryResult = queryGetTimeOfDayInternetFastestSlowest();

	// Fail the resource request if their was an issue executing the query.
	if (queryResult.code != QUERY_SUCCESS){
		// Map the query result code to a REST exit code and produce an error
		// message.
		body = queryResult.err;
		exitCode = QRC_TO_HTTP_CODE.find(queryResult.code)->second;
	}
	else{
		body = queryResult.factualStr;
		exitCode = Http::Code::Ok;
	}

	response.send(exitCode, body);
}

/*
 * Service resource call for factual query off
 * 'Information about the ISP performance'.
 */
void DebugnetRestAPI::runGetIspPerformance(
		const Rest::Request& request, Http::ResponseWriter response){
	Http::Code exitCode;
	string body;

	response.headers().add<Http::Header::ContentType>(MIME(Text, Plain));

	double expUpSpeed = request.param(":expected-up").as<double>();
	double expDownSpeed = request.param(":expected-down").as<double>();

	// Run factual query
	FactualQueryResult queryResult = queryGetIspPerformance(expUpSpeed, expDownSpeed);

	// Fail the resource request if their was an issue executing the query.
	if (queryResult.code != QUERY_SUCCESS){
		// Map the query result code to a REST exit code and produce an error
		// message.
		body = queryResult.err;
		exitCode = QRC_TO_HTTP_CODE.find(queryResult.code)->second;
	}
	else{
		body = queryResult.factualStr;
		exitCode = Http::Code::Ok;
	}

	response.send(exitCode, body);
}

/*
 * Service request to get devices on the network as discovered by DebugNet
 */
void DebugnetRestAPI::getDevices(const Rest::Request& request,
			Http::ResponseWriter response){
	response.headers().add<Http::Header::ContentType>(MIME(Application, Json));

	Document allDevicesJson;
	allDevicesJson.SetArray();
	Document::AllocatorType& allocator = allDevicesJson.GetAllocator();

	for (Device device : getAllDevices()){
		Value deviceJson(kObjectType);

		// Add MAC address
		deviceJson.AddMember(
				"mac",
				Value(device.getMacAddress().c_str(), allocator),
				allocator);

		// Add the human name of the device
		deviceJson.AddMember(
				"human_name",
				Value(device.getHumanReadableName().c_str(), allocator),
				allocator);

		// Add status of connection. Enum is converted to its respective ID.
		deviceJson.AddMember(
				"status",
				Value(deviceStatusEnumToIntId(device.getStatus())),
				allocator);

		// Add the connection type
		deviceJson.AddMember(
				"conn_type",
				Value(connTypeEnumToIntId(device.getConnectionType())),
				allocator);

		// Get first time connected in unix time
		deviceJson.AddMember(
				"first_connected",
				Value(static_cast<int>(device.getFirstConnectedTime())),
				allocator);

		// Get first time connected in unix time
		deviceJson.AddMember(
				"last_connected",
				Value(static_cast<int>(device.getLastConnectedTime())),
				allocator);

		// Initialise members which could be null if nothing is set for it
		Value hostname;
		Value ipAddress;
		Value manufacturer;

		if (device.hasHostname()){
			hostname.SetString(device.getHostname().c_str(), allocator);
		}

		if (device.hasIpAddress()){
			ipAddress.SetString(device.getIpAddress().c_str(), allocator);
		}

		if (device.hasManufacturer()){
			manufacturer.SetString(device.getManufacturer().c_str(), allocator);
		}

		deviceJson.AddMember(
				"hostname",
				hostname,
				allocator);

		deviceJson.AddMember(
				"ip_address",
				ipAddress,
				allocator);

		deviceJson.AddMember(
				"manufacturer",
				manufacturer,
				allocator);

		// Include some metric data for device if measure time given
		if (request.hasParam(":measure-time")){
			int secondsToLookBack = request.param(":measure-time").as<int>();

			// Set lookup parameters
			vector<string> devFilter = {"device=\'"+device.getMacAddress()+"\'"};
			int timeNow = static_cast<int>(time(0));
			int timeFrom = timeNow - secondsToLookBack;

			Value downSpeed;
			Value upSpeed;
			Value signalStrength;

			double avgMetricVal;

			// Get avg download speed for device. If not found then leave as null
			if(getAvgMetricFieldValue(TRAFFIC_LOAD_DEVICES_TABLE, "bps_in",
					timeFrom, timeNow, avgMetricVal, devFilter) == 0){
				downSpeed.SetFloat(bitsToMegaBits(bytesToBits(avgMetricVal)));
			}

			deviceJson.AddMember(
					"download_speed",
					downSpeed,
					allocator);

			// Get avg upload speed for device. If not found then leave as null
			if(getAvgMetricFieldValue(TRAFFIC_LOAD_DEVICES_TABLE, "bps_out",
					timeFrom, timeNow, avgMetricVal, devFilter) == 0){
				upSpeed.SetFloat(bitsToMegaBits(bytesToBits(avgMetricVal)));
			}

			deviceJson.AddMember(
					"upload_speed",
					upSpeed,
					allocator);

			// Get avg signal strength
			if(getAvgMetricFieldValue(WIRELESS_SIGNAL_TABLE, "signal",
					timeFrom, timeNow, avgMetricVal, devFilter) == 0){
				signalStrength.SetFloat(avgMetricVal);
			}

			deviceJson.AddMember(
					"signal_strength",
					signalStrength,
					allocator);
		}


		allDevicesJson.PushBack(deviceJson, allocator);
	}

	// Convert the Json object into a Json string
	StringBuffer buffer;
    Writer<StringBuffer> writer(buffer);
    allDevicesJson.Accept(writer);
    string allDevicesJsonStr = string(buffer.GetString());

    response.send(Http::Code::Ok, allDevicesJsonStr);
}

