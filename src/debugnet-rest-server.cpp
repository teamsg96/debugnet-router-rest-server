/*
 * debugnet-rest-server.cpp
 *
 *  Created on: 7 Mar 2019
 *      Author: sgasper
 */
#include "debugnet-rest-server.h"

/*
 * Setup each resource the server exposes and bind them to functions that will
 * service them.
 */
Rest::Router setupRoutes(DebugnetRestAPI dbApi){
	Rest::Router router;
	Routes::Get(router, "/query/troubleshoot/conn-drop/:device-mac/:instable-time",
			Routes::bind(&DebugnetRestAPI::runConnectionDropTS, &dbApi));
	Routes::Get(router, "/query/troubleshoot/slow-device/:device-mac/:slow-time",
			Routes::bind(&DebugnetRestAPI::runSlowDeviceTS, &dbApi));
	Routes::Get(router, "/query/troubleshoot/slow-network/:slow-time",
			Routes::bind(&DebugnetRestAPI::runSlowNetworkTS, &dbApi));
	Routes::Get(router, "/query/factual/current-internet-speed",
			Routes::bind(&DebugnetRestAPI::runGetCurrentInternetSpeed, &dbApi));
	Routes::Get(router, "/query/factual/tod-internet-slowest-fastest",
			Routes::bind(&DebugnetRestAPI::runGetTimeOfDayInternetFastestSlowest, &dbApi));
	Routes::Get(router, "/query/factual/isp-performance/:expected-up/:expected-down",
			Routes::bind(&DebugnetRestAPI::runGetIspPerformance, &dbApi));
	Routes::Get(router, "/devices",
			Routes::bind(&DebugnetRestAPI::getDevices, &dbApi));
	Routes::Get(router, "/devices/:measure-time",
			Routes::bind(&DebugnetRestAPI::getDevices, &dbApi));

	return router;
}

int main() {
	DebugnetRestAPI dbApi;

	cout << "Starting DebugNet REST server..." << endl;

	// Setup resource routes and the functions that will handle each
	Rest::Router router = setupRoutes(dbApi);

	// Bind server to address and port
    Pistache::Address addr(Pistache::Ipv4::any(), Pistache::Port(PORT));
    Http::Endpoint server(addr);

    // Set settings for the server end point
    auto opts = Pistache::Http::Endpoint::options()
        .threads(1).flags(Tcp::Options::InstallSignalHandler |
        		Tcp::Options::ReuseAddr);
    server.init(opts);

    // Set the request handler
    server.setHandler(router.handler());

    cout << "REST server started." << endl;
    // Start server
    server.serve();

    // Shutdown server
    server.shutdown();
}

